import React, { Component } from 'react';

import classes from './App.css';
import Radium from 'radium';
import Person from './Person/Person';

class App extends Component {
    state = {
        persons: [
            {id:'123',name: "Max", Age: 28},
            {id:'1234',name: "manu", Age: 29},
            {id:'132',  name:"Stephine",Age:26}
        ],
        otherState: "Some Value",
        showpersons:false
    };
    switchNameHandler = (Newname) => {
        this.setState(
            {
                persons: [
                    {name: Newname, Age: 28},
                    {name: "manu", Age: 29},
                    {name: 'Stephine', Age: 26}
                ]
            }
        )
    };
    deleteNameHandler =(personIndex) =>{
        const persons=this.state.persons;
        persons.splice(personIndex,1);
        this.setState({persons:persons});


    }


    nameChangedHandler = (event) => {
        this.setState({
                persons: [
                    {name: 'Max', Age: 28},
                    {name: event.target.value, Age: 29},
                    {name: 'Stephine', Age: 26}
                ]

            }
        )
    };
    toggleNameHandler = () =>{
        const doesShow=this.state.somevalue;
        this.setState({somevalue: !doesShow});


    }
    render(){
        const style={
            backgroundColor:'green',
            color:'white',
            font:'inherit',
            padding:'16px',
            ':hover':{
                backgroundColor:'lightgrey',
                color:'black'
            }
        }
        var persons = null;
        var person;
        if(this.state.somevalue) {
            person = <div>
                {this.state.persons.map((person,index) =>
                {
                    return <Person
                        click={() => this.deleteNameHandler(index)}
                        name={person.name}
                        Age={person.Age}
                        key={person.id}
                        changed={(event) => this.nameChangedHandler(event,person.id)}/>
                })}

            </div>;}
        style.backgroundColor='Red';
        let classes=['red','bold'].join(' ');
        style[':hover']={
            backgroundColor:'lightred',
            color:'black'
        };
        {
            return (
                <div className={classes.App}>
                    <h1>This is heading</h1>
                    <p className={classes}>This is paragraph</p>
                    <button
                        style={style}
                        onClick={this.toggleNameHandler}> click</button>
                    {person}



                </div>
            )
        }}
}


export default Radium (App);