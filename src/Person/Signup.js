import React,{Component} from 'react';
import ReactDom from 'react-dom';

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            FirstName: '',
            LastName: '',
            Age: '',
            Email: '',
            PhoneNo: ''
        }

    }

    SwitchSignup = () => {

    }

    render() {
        const style = {
            textAlign:'centre',
            backgroundColor: 'white',
            color: 'black',
        };
        return (
            <div>
                <AppBar title="Signup"/>
                <h5>FirstName</h5>
                <input type="text"
                       onChange={(event, newValue) => this.setState({FirstName: newValue})}
                />
                <h5>Last Name</h5>
                <input type="text"
                       onChange={(event, newValue) => this.setState({LastName: newValue})}/>
                <h5>Age:</h5>
                <input type="number"
                       onChange={(event, newValue) => this.setState({Age: newValue})}/>
                <h5>Email </h5>
                <input type="text"
                       onChange={(event, newValue) => this.setState({Email: newValue})}/>
                <h5>Phone No:</h5>
                <input type="number"
                       onChange={(event, newValue) => this.setState({PhoneNo: newValue})}/>
                <h5>Create Password</h5>
                <input type="text"/>
                <h5>Confirm Password</h5>
                <input type="text"/>
                <RaisedButton label="Submit" primary={true}
                              onClick={(event) => this.handleClick(event)}/>
            </div>
        )
    }
}
export default App;