import React,{Component} from 'react';
import ReactDom from 'react-dom';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            FirstName: '',
            LastName: '',
            Age: '',
            Email: '',
            PhoneNo: ''
        }


        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    //event.preventDefault();
    render() {

        const style = {
            backgroundColor: 'white',
            color: 'black',
        }

        return (
            <div>
                <h5>FirstName</h5>
                <input type="text"
                       onChange={this.handleChange} value={this.state.FirstName}/>
                <h5>LastName</h5>
                <input type="text"
                       onChange={this.handleChange} value={this.state.LastName}/>
                <h5>Age:</h5>
                <input type="number" onChange={this.handleChange}/>
                <button onChange={this.handleChange} >sign up</button>
            </div>
        )

    }
}
export default App;