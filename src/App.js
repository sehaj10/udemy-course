import React,{Component} from 'react';
import ReactDom from 'react-dom';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            FirstName: ' ',
            LastName: ' ',
            Age: ' ',
            Email: ' ',
            PhoneNo: ' '
        }

    }

    handleClick = () => {
        console.log('Signup Function Click');

    };
    onChange = (event,key) => {
        let value=event.target.value


    };

    render() {
        const style = {
            textAlign:'centre',
            backgroundColor: 'white',
            color: 'black',
        };
        return (
            <div>
                <appbar title="Signup"/>
                <h5>FirstName</h5>
                <input type="text"
                       onChange={(event) => this.onChange(event,"FirstName")}
                />
                <h5>Last Name</h5>
                <input type="text"
                       onChange={(event) => this.onChange(event,"LastName")}/>
                <h5>Age:</h5>
                <input type="number"
                       onChange={(event) => this.onChange(event,"Age")}/>
                <h5>Email </h5>
                <input type="text"
                       onChange={(event) => this.onChange(event,"Email")}/>
                <h5>Phone No:</h5>
                <input type="number"
                       onChange={(event) => this.onChange(event,"PhoneNo")}/>
                <h5>Create Password</h5>
                <input type="text"/>
                <h5>Confirm Password</h5>
                <input type="text"/>
                <button
                    onClick={(event) => this.handleClick(event)}>Sign up</button>
            </div>
        )
    }
}
export default App;
